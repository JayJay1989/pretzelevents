<p align="center"> 
	<img src="https://lateur.pro/dev/pretzelEvents.png" style="max-height: 300px;">
</p>

<p align="center">
	<a href="https://www.microsoft.com/net"><img src="https://img.shields.io/badge/.NET%20Standard-2.0-orange.svg" style="max-height: 300px;"></a>
	<img src="https://img.shields.io/badge/Platform-.NET-lightgrey.svg" style="max-height: 300px;" alt="Platform: .net">
</p>

## About
PretzelEvent is a C# library that allow interaction with Pretzel API. Current supported services are: currently plaing song. Below you can find the instruction on how to use it.

## Features
* **PretzelApi**:
    * Get Raw or formatted information of the currently playing song

## Documentation
#### Doxygen
Coming soon

## Implementing
#### PretzelEvent.Lib.PretzelApi - CSharp
```csharp
using System;
using PretzelEvents.Lib;
using PretzelEvents.Lib.Events;

namespace PretzelEvents.Cons
{
    class Program
    {
        static void Main(string[] args)
        {
            Bot bot = new Bot();
            Console.ReadLine();
        }
    }

    class Bot
    {
        public Bot()
        {
            PretzelApi pretzel = new PretzelApi("JayJay1989BE");
            pretzel.OntTitleChangeEventHandler += OntTitleChangeEventHandler;
            pretzel.UseTwitch();
            pretzel.SetRequestInterval(1000);
            pretzel.Start();
            //or
            //pretzel.UseMixer().Start();
        }

        private void OntTitleChangeEventHandler(object sender, onTitleChangeArgs e)
        {
            Console.WriteLine($"JayJay is currently listening to: {e.PretzelSong.Title} by {e.PretzelSong.Artist}");
            Console.WriteLine($"Url: {e.PretzelSong.Url}");
            Console.WriteLine($"Raw: {e.PretzelSong.RAW}");
        }
    }
}
```

or in the [Console project](https://gitlab.com/JayJay1989/pretzelevents/PretzelEvents.Cons)

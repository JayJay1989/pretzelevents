﻿using System;
using PretzelEvents.Lib;
using PretzelEvents.Lib.Events;

namespace PretzelEvents.Cons
{
    class Program
    {
        static void Main(string[] args)
        {
            Bot bot = new Bot();
            Console.ReadLine();
        }
    }

    class Bot
    {
        public Bot()
        {
            PretzelApi pretzel = new PretzelApi("JayJay1989BE");
            pretzel.OntTitleChangeEventHandler += OntTitleChangeEventHandler;
            pretzel.UseTwitch();
            pretzel.SetRequestInterval(1000);
            pretzel.Start();
            //or
            //pretzel.UseMixer().Start();
        }

        private void OntTitleChangeEventHandler(object sender, onTitleChangeArgs e)
        {
            Console.WriteLine($"JayJay is currently listening to: {e.PretzelSong.Title} by {e.PretzelSong.Artist}");
            Console.WriteLine($"Url: {e.PretzelSong.Url}");
            Console.WriteLine($"Raw: {e.PretzelSong.RAW}");
        }
    }
}

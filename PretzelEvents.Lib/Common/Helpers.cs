﻿using System.Text.RegularExpressions;
using PretzelEvents.Lib.Models;

namespace PretzelEvents.Lib.Core.Helpers
{
    /// <summary>
    /// Static class of helper functions used around the project.
    /// </summary>
    public static class Helpers
    {
        /// <summary>
        /// Parse the raw text of the webrequest
        /// </summary>
        /// <param name="text">RAW Data</param>
        /// <returns><seealso cref="PretzelSong"/> the PretzelSong</returns>
        public static PretzelSong ParsePretzelSong(string text)
        {
            Regex r = new Regex(@"Now Playing: (.*w?) by (.*w?) -> (.*w?)");
            MatchCollection mc = r.Matches(text);
            PretzelSong returnSong = new PretzelSong
            {
                RAW = text,
                Title = mc[0].Groups[1].Value,
                Artist = mc[0].Groups[2].Value,
                Url = mc[0].Groups[3].Value
            };
            return returnSong;
        }
    }
}

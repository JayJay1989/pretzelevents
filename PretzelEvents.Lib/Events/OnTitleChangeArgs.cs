﻿using System;
using PretzelEvents.Lib.Models;

namespace PretzelEvents.Lib.Events
{
    /// <inheritdoc />
    /// <summary>
    /// Object representing the arguments for a title change event
    /// </summary>
    public class onTitleChangeArgs : EventArgs
    {
        /// <summary>
        /// Property representing the information of the new song playing on Pretzel.
        /// </summary>
        public PretzelSong PretzelSong { get; set; }

    }
}

﻿using System;
using System.Net;
using PretzelEvents.Lib.Models;

namespace PretzelEvents.Lib.Core
{
    /// <summary>
    /// Class representing WebRequest
    /// </summary>
    class WebRequest
    {
        private PretzelSong _pretzelSong;
        private string _url;

        /// <summary>
        /// Constructor for creating a web request
        /// </summary>
        /// <param name="url"></param>
        public WebRequest(string url)
        {
            this._url = url;
            makeRequest();
        }

        /// <summary>
        /// Make a request
        /// </summary>
        private void makeRequest()
        {
            using (WebClient client = new WebClient() { Encoding = System.Text.Encoding.UTF8 })
            {
                string s = null;
                try
                {
                    s = client.DownloadString(_url);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                if (s == null) return;
                this._pretzelSong = Helpers.Helpers.ParsePretzelSong(s);
            }
        }

        /// <summary>
        /// Get the result
        /// </summary>
        /// <returns>The result</returns>
        public PretzelSong GetResult()
        {
            return this._pretzelSong;
        }
    }
}

﻿namespace PretzelEvents.Lib.Models
{
    /// <summary>
    /// Model representing the pretzel song.
    /// </summary>
    public class PretzelSong
    {
        /// <summary>
        /// Artist of the current song
        /// </summary>
        /// <value>The Artist</value>
        public string Artist { get; set; }

        /// <summary>
        /// Title of the current song
        /// </summary>
        /// <value>The Title</value>
        public string Title { get; set; }

        /// <summary>
        /// Pretzel url of the current song
        /// </summary>
        /// <value>The url</value>
        public string Url { get; set; }

        /// <summary>
        /// Raw data
        /// </summary>
        /// <value>The Raw data</value>
        public string RAW { get; set; }
    }
}

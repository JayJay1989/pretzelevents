﻿using System;
using System.Timers;
using Microsoft.Extensions.Logging;
using PretzelEvents.Lib.Core;
using PretzelEvents.Lib.Events;

namespace PretzelEvents.Lib
{
    /// <summary>
    /// Class represneting interactions with the Pretzel Events
    /// </summary>
    public class PretzelApi
    {
        /// <summary>
        /// The username
        /// </summary>
        private string _username;
        /// <summary>
        /// Use Twitch
        /// </summary>
        private bool _isTwitch;
        /// <summary>
        /// Use Mixer
        /// </summary>
        private bool _isMixer;
        /// <summary>
        /// The timer interval
        /// </summary>
        private int _timerInterval = 1000;
        /// <summary>
        /// The logger
        /// </summary>
        private readonly ILogger<PretzelApi> _logger;
        /// <summary>
        /// The old url
        /// </summary>
        private string _oldUrl;
        /// <summary>
        /// The ping timer
        /// </summary>
        private Timer timer = new Timer();

        #region Events
        /// <inheritdoc />
        /// <summary>
        /// Fires when PretzelApi receives an update of a song from Pretzel
        /// </summary>
        public EventHandler<onTitleChangeArgs> OntTitleChangeEventHandler;

        #endregion


        /// <summary>
        /// Constructor for a client that interface's with Pretzel Events system.
        /// </summary>
        /// <param name="username">The Twitch/Mixer username</param>
        /// <param name="logger">Optional ILogger param to enable logging</param>
        public PretzelApi(string username, ILogger<PretzelApi> logger = null)
        {
            this._username = username;
            _logger = logger;
        }

        /// <inheritdoc />
        /// <summary>
        /// Make requests for a Twitch Username
        /// </summary>
        /// <returns></returns>
        public PretzelApi UseTwitch()
        {
            _isTwitch = true;
            _isMixer = false;
            return this;
        }

        /// <inheritdoc />
        /// <summary>
        /// Make requests for a Mixer Username
        /// </summary>
        /// <returns></returns>
        public PretzelApi UseMixer()
        {
            _isTwitch = false;
            _isMixer = true;
            return this;
        }

        /// <inheritdoc />
        /// <summary>
        /// Method to modify the interval
        /// </summary>
        /// <param name="interval"></param>
        public void SetRequestInterval(int interval = 1000)
        {
            _timerInterval = interval;
        }

        /// <summary>
        /// Method to start the timer
        /// </summary>
        public void Start()
        {
            timer.Interval = _timerInterval;
            timer.Elapsed += WebRequestTimerTick;
            timer.Start();
        }

        /// <inheritdoc />
        /// <summary>
        /// Method to stop the timer
        /// </summary>
        public void Stop()
        {
            timer.Stop();
        }

        /// <summary>
        /// Timer tick event
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The <see cref="ElapsedEventArgs"/> instance containing the event data.</param>
        private void WebRequestTimerTick(object sender, ElapsedEventArgs e)
        {
            WebRequest webRequest;
            if (_isTwitch && !_isMixer) webRequest = new WebRequest("https://www.pretzel.rocks/api/v1/playing/twitch/" + _username);
            else webRequest = new WebRequest("https://www.pretzel.rocks/api/v1/playing/mixer/" + _username);

            if (webRequest.GetResult() == null) return;

            if (_oldUrl != webRequest.GetResult().Url)
                OntTitleChangeEventHandler?.Invoke(this, new onTitleChangeArgs 
                {
                    PretzelSong = webRequest.GetResult()
                });
            _oldUrl = webRequest.GetResult().Url;
        }
    }
}